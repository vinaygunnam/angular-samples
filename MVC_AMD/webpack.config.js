﻿var webpack = require('webpack');
var path = require('path');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    debug: true,
    context: path.join(__dirname, "Scripts"),
    entry: {
        app: ['./app/app']
    },
    output: {
        path: path.join(__dirname, "Scripts/bundles"),
        filename: '[name].js',
        publicPath: '/Scripts/bundles/'
    },
    module: {
        preLoaders: [
            { test: /\.js$/, loader: 'baggage?[file].html&[file].css' }
        ],
        loaders: [
            { test: /[\/\\]jquery-1.10.2\.js$/, loader: "exports?jQuery" },
            { test: /[\/\\]angular\.js$/, loader: "exports?angular" },
            { test: /\.html$/, loader: "ngtemplate?relativeTo=" + __dirname + "/!html" },
            { test: /\.css$/, loader: "style!css" }
        ]
    },
    plugins: [
      new BrowserSyncPlugin({
          proxy: "localhost:48903"
      })
    ],
    resolve: {
        root: [
            path.join(__dirname, "Scripts")
        ],
        alias: {
            "angular": "angular"
        }
    }
};