﻿import angular = require('angular');
import counter = require('./components/counter/counter');

export = angular.module('app', [])
    .directive('counter', counter)