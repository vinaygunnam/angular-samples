﻿class CounterController {
    num: number = 0;

    increment(num: number): void {
        this.num = num + 2;
    }

    decrement(num: number): void {
        this.num = num - 1;
    }
}

export = CounterController;