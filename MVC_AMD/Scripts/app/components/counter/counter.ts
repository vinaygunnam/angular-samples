﻿import CounterController = require('./CounterController');

require('./counter.css');

function counter(): ng.IDirective {
    var directive: ng.IDirective = {
        controller: CounterController,
        controllerAs: 'cc',
        templateUrl: require('./counter.html'),
        //template: '<h1>Testttttt</h1>',
        replace: true
        //scope: {
        //    num: '=seed'
        //},
        //bindToController: true
    };

    return directive;
}

export = counter;