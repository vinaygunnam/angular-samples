define(["require", "exports"], function (require, exports) {
    var CounterController = (function () {
        function CounterController() {
            this.num = 0;
        }
        CounterController.prototype.increment = function (num) {
            this.num = num + 2;
        };
        CounterController.prototype.decrement = function (num) {
            this.num = num - 1;
        };
        return CounterController;
    })();
    return CounterController;
});
//# sourceMappingURL=CounterController.js.map