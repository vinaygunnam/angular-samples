define(["require", "exports", './CounterController'], function (require, exports, CounterController) {
    require('./counter.css');
    function counter() {
        var directive = {
            controller: CounterController,
            controllerAs: 'cc',
            templateUrl: require('./counter.html'),
            //template: '<h1>Testttttt</h1>',
            replace: true
        };
        return directive;
    }
    return counter;
});
//# sourceMappingURL=counter.js.map