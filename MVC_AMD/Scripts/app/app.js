define(["require", "exports", 'angular', './components/counter/counter'], function (require, exports, angular, counter) {
    return angular.module('app', [])
        .directive('counter', counter);
});
//# sourceMappingURL=app.js.map